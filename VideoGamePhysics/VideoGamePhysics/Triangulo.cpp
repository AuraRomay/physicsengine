#include "Triangulo.h"
#include <math.h>
#include <iostream>

//punto arriba= x, y + 
//radio = 2h/3
//h= b/2

Triangulo::Triangulo(SDL_Renderer *rend, CVector3 centro, int size) :Primitive(rend, centro, size)
{
	
}

void Triangulo::getVertices()
{
	float altura = sqrt(pow( size,2) - pow( (size/2),2));
	float radio = (2 * altura) / 3;
	float ap = altura - radio;
	float newSi = size / 2;

	CVector3 punto1(position.x(), position.y() - (altura/2), 0);
	CVector3 punto2(position.x() - newSi, position.y() +(altura/2), 0);
	CVector3 punto3(position.x() + (size/2), position.y() + (altura / 2), 0);

	vertices.push_back(punto1);
	vertices.push_back(punto2);
	vertices.push_back(punto3);
}


Triangulo::~Triangulo()
{
}
