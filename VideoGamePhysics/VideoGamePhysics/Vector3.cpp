#include "Vector3.h"



CVector3::CVector3()
{
	puntos[0] = 0;
	puntos[1] = 0;
	puntos[2] = 0;
}

CVector3::CVector3(float x, float y, float z)
{
	puntos[0] = x;
	puntos[1] = y;
	puntos[2] = z;
}

void CVector3::setPunto(float x, float y, float z)
{
	this->puntos[0] = x;
	this->puntos[1] = y;
	this->puntos[2] = z;
}

CVector3 CVector3::operator+(const CVector3 & vec)
{

	return CVector3(this->puntos[0] + vec.puntos[0], this->puntos[1] + vec.puntos[1], this->puntos[2] + vec.puntos[2]);
}

CVector3 CVector3::operator-(const CVector3 & vec)
{
	return CVector3(this->puntos[0] - vec.puntos[0], this->puntos[1] - vec.puntos[1], this->puntos[2] - vec.puntos[2]);
}

CVector3 CVector3::operator*(const CVector3 & vec)
{
	CVector3 cruz = CVector3();
	cruz.puntos[0]= ((this->puntos[1] * vec.puntos[2]) - (this->puntos[2] * vec.puntos[1]));
	cruz.puntos[1] = -1 *((this->puntos[0] * vec.puntos[2]) - (this->puntos[2] * vec.puntos[0]));
	cruz.puntos[2] = ((this->puntos[0] * vec.puntos[1]) - (this->puntos[1] * vec.puntos[0]));
	return cruz;
}

CVector3 CVector3::operator*(const float & num)
{
	return CVector3(this->puntos[0] * num, this->puntos[1] * num, this->puntos[2] * num);
}

CVector3 CVector3::proyVectorial(CVector3 vec)
{
	CVector3 temp = CVector3();
	temp.setPunto(this->puntos[0], this->puntos[1], this->puntos[2]);
	float resPunto = temp.pPunto(vec);
	float div = temp.pPunto(temp);
	
	float escalar = resPunto / div;

	CVector3 resultado = CVector3();
	resultado = temp * escalar;
	return resultado;
}

void CVector3::operator+=(const CVector3 & vec)
{
	CVector3 temp = CVector3();
	temp.puntos[0] = this->puntos[0] + vec.puntos[0];
	temp.puntos[1] = this->puntos[1] + vec.puntos[1];
	temp.puntos[2] = this->puntos[2] + vec.puntos[2];

	std::cout << temp.puntos[0] << ", " << temp.puntos[1] << ", " << temp.puntos[2] << std::endl;
}

void CVector3::operator-=(const CVector3 & vec)
{
	CVector3 temp = CVector3();
	temp.puntos[0] = this->puntos[0] - vec.puntos[0];
	temp.puntos[1] = this->puntos[1] - vec.puntos[1];
	temp.puntos[2] = this->puntos[2] - vec.puntos[2];

	std::cout << temp.puntos[0] << ", " << temp.puntos[1] << ", " << temp.puntos[2] << std::endl;
}

void CVector3::operator*=(const CVector3 & vec)
{
	CVector3 cruz = CVector3();
	cruz.puntos[0] = ((this->puntos[1] * vec.puntos[2]) - (this->puntos[2] * vec.puntos[1]));
	cruz.puntos[1] = -((this->puntos[0] * vec.puntos[2]) - (this->puntos[2] * vec.puntos[0]));
	cruz.puntos[2] = ((this->puntos[0] * vec.puntos[1]) - (this->puntos[1] * vec.puntos[0]));

	std::cout << cruz.puntos[0] << ", " << cruz.puntos[1] << ", " << cruz.puntos[2] << std::endl;
}

void CVector3::operator*=(const float & num)
{
	CVector3 temp = CVector3();
	temp.puntos[0] = this->puntos[0] * num;
	temp.puntos[1] = this->puntos[1] * num;
	temp.puntos[2] = this->puntos[2] * num;

	std::cout << temp.puntos[0] << ", " << temp.puntos[1] << ", " << temp.puntos[2] << std::endl;
}

float CVector3::proyEscalar(CVector3 vec)
{
	CVector3 temp = CVector3();
	CVector3 res = CVector3();
	temp.setPunto(this->puntos[0], this->puntos[1], this->puntos[2]);

	res = temp.proyVectorial(vec);
	float resultado = res.magnitud();

	return resultado;
}

float CVector3::pPunto(const CVector3 & vec)
{
	float a;
	a = (this->puntos[0] * vec.puntos[0]) + 
		(this->puntos[1] * vec.puntos[1]) + 
		(this->puntos[2] * vec.puntos[2]);
	return a;
}

float CVector3::magnitud()
{
	float m;
	m = sqrt(pow(this->puntos[0], 2) + pow(this->puntos[1], 2) + pow(this->puntos[2], 2));
	return m;
}

CVector3 CVector3::normalizar()
{
	if (this->puntos[0] == 0 && this->puntos[1] == 0 && this->puntos[2] == 0) {
		std::cout << "NO SE PUEDE" << std::endl;
		return CVector3();
	}
	else {
		float m;
		m = sqrt(pow(this->puntos[0], 2) + pow(this->puntos[1], 2) + pow(this->puntos[2], 2));

		CVector3 tempV;
		tempV.puntos[0] = this->puntos[0] / m;
		tempV.puntos[1] = this->puntos[1] / m;
		tempV.puntos[2] = this->puntos[2] / m;

		return tempV;
	}
}

void CVector3::imprimir()
{
	std::cout << puntos[0] << ", " << puntos[1] << ", " << puntos[2] << std::endl;
}

CVector3::~CVector3()
{

}

