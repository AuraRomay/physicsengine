#pragma once
#include "Vector3.h"

class Matrix3
{
public:
	Matrix3();
	~Matrix3();

	Matrix3(CVector3 v1, CVector3 v2, CVector3 v3);
	CVector3 vec[3];
	CVector3 v1() { return vec[0]; }
	CVector3 v2() { return vec[1]; }
	CVector3 v3() { return vec[2]; }

	void mTranspose();
	CVector3 operator*(const CVector3 & vectorMul);
	Matrix3 operator*(const Matrix3 & matrizMul);
};

