#include <iostream>
#include <SDL.h>
#include <stdio.h>
#include <math.h>
#include "Timer.h"
#include "Input.h"
#include "Vector3.h"
#include "Matrix3.h"
#include "Particula.h"
#include "Primitive.h"
#include "Cube.h"
#include "Cuadrado.h"
#include "Triangulo.h"
#include "Tetaedro.h"
#include "Resortes.h"
#include "Quaternion.h"
using namespace std;

const int WIDTH = 500, HEIGHT = 256;

SDL_Renderer* renderer = NULL;
CVector3 gravedad(0, 9.81, 0);
float inputForce = 5000;
Quaternion Quaternion::identity = Quaternion(CVector3(1, 0, 0), 0);
void Prueba() {
	CVector3 vec1(3, 1, 0);
	CVector3 vec2(1, 5, 0);
	float mul = 5;
	CVector3 res;
	res = vec1.proyVectorial(vec2);

	/*res = vec1 + vec2;
	res.imprimir();
	res = vec1 - vec2;
	res.imprimir();
	res = vec1 * vec2;
	res.imprimir();
	res = vec1 * mul;
	res.imprimir();

	std::cout<<vec1.pPunto(vec2)<<std::endl;
	vec1.magnitud();
	vec1.imprimir();
	vec1.normalizar();
	vec1.imprimir();*/
}

void Mat() {
	CVector3 vec1(2, 3, 5);
	CVector3 vec2(0, 0, 1);
	CVector3 vec3(1, 0, 1);

	CVector3 vec4(1, 1, 1);
	CVector3 vec5(0, 2, 1);
	CVector3 vec6(1, 1, 0);
	CVector3 res;

	Matrix3 mat1(vec1, vec2, vec3);
	Matrix3 mat2(vec4, vec5, vec6);
	Matrix3 resM;
	res = mat1 * vec4;
	resM = mat1 * mat2;
}

void pixel() {
	CParticula particula;
	CParticula tan;
	particula.set_pixel(renderer, 5, 10, 0, 255, 0, 255);

}

int main(int argc, char *argv[])


{


	/*CParticula *par  = new CParticula( 1, CVector3(50, 100, 0), CVector3(0, 0, 0));
	CParticula *par2 = new CParticula(5, CVector3(50, 150, 0), CVector3(0, 0, 0));*/
	//ResorteA r1 = ResorteA(200, 0, CVector3(30, 30, 0), par);
	//ResorteB r2 = ResorteB(200, 0, par, par2);
	CParticula par = CParticula(1, CVector3(50, 100, 0), CVector3(0, 0, 0));
	CParticula par2 = CParticula(5, CVector3(50, 150, 0), CVector3(0, 0, 0));
	ResorteA r1 = ResorteA(20, 1, CVector3(30, 30, 0), par);
	ResorteB r2 = ResorteB(20, 1, par, par2);
	/*Quaternion rot = Quaternion(CVector3(1, 0, 0), .005*3.1416/180);*/
	Quaternion rot = Quaternion(CVector3(1, 1, 1), 0.01);
	//rot.real = cos(rot.real);
	//rot.ijk = rot.ijk * sin(rot.real);

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		cout << "SDL could not initialize! SDL Error: " << SDL_GetError() << endl;
	}

	SDL_Window *window = SDL_CreateWindow("Physics Engine", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_ALLOW_HIGHDPI);

	if (window == NULL)
	{
		cout << "Could not create window: " << SDL_GetError() << endl;
		return EXIT_FAILURE;
	}

	SDL_Event windowEvent;
	renderer = SDL_CreateRenderer(window, -1, 0);
	cout << renderer << endl;
	float position = 0;
	Prueba();
	Mat();
	
	Cube cubo(renderer, CVector3(WIDTH/2, HEIGHT/2, 0), 100);
	Cube cube(renderer, CVector3(100, 100, 10), 50);
	cube.getVertices();
	cubo.getVertices();

	
	while (true)
	{
		if (SDL_PollEvent(&windowEvent))
		{
			if (windowEvent.type == SDL_QUIT)
			{
				break;
			}

			Input::Update(windowEvent);
		}

		Timer::Update();
		
		/*if (Input::GetKey(SDLK_a)) {
			par->addForce(CVector3(-inputForce, 0, 0));
			par2->addForce(CVector3(-inputForce, 0, 0));
		}
		if (Input::GetKey(SDLK_d)) {
			par->addForce(CVector3(inputForce, 0, 0));
			par2->addForce(CVector3(inputForce, 0, 0));
		}
		if (Input::GetKey(SDLK_s)) {
			par->addForce(CVector3(0, inputForce, 0));
			par2->addForce(CVector3(0, inputForce, 0));
		}
		if (Input::GetKey(SDLK_w)) {
			par->addForce(CVector3(0, -inputForce, 0));
			par2->addForce(CVector3(0, -inputForce, 0));
		}*/

		if (Input::GetKey(SDLK_a)) {
			par.addForce(CVector3(-inputForce, 0, 0));
			par2.addForce(CVector3(-inputForce, 0, 0));
			Quaternion rot = Quaternion(CVector3(1, 0, 0), 1);
			cubo.QRotate(rot);
		}
		if (Input::GetKey(SDLK_d)) {
			par.addForce(CVector3(inputForce, 0, 0));
			par2.addForce(CVector3(inputForce, 0, 0));
			Quaternion rot = Quaternion(CVector3(-1, 0, 0), 1);
			cubo.QRotate(rot);
		}
		if (Input::GetKey(SDLK_s)) {
			par.addForce(CVector3(0, inputForce, 0));
			par2.addForce(CVector3(0, inputForce, 0));
			Quaternion rot = Quaternion(CVector3(0, -1, 0), 1);
			cubo.QRotate(rot);
		}
		if (Input::GetKey(SDLK_w)) {
			par.addForce(CVector3(0, -inputForce, 0));
			par2.addForce(CVector3(0, -inputForce, 0));
			Quaternion rot = Quaternion(CVector3(0, 1, 0), 1);
			cubo.QRotate(rot);
		}

		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
		SDL_RenderClear(renderer);
	
		par.addForce(gravedad*par.masa);
		par2.addForce(gravedad*par2.masa);

		r1.fuerzaRe();
		r2.fuerzaRe2();

		par.Integrate(Timer::GetDeltaTime());
		par2.Integrate(Timer::GetDeltaTime());
		//par.set_pixel(renderer,par.pos.puntos[0],par.pos.puntos[1],255, 0 , 0,0);
		Quaternion perro = Quaternion::identity;

		//position += Timer::GetDeltaTime();
		float time = Timer::GetTotalTime();
		par.set_pixel(renderer, par.pos.puntos[0], par.pos.puntos[1], 255, 0, 0, 0);
		par2.set_pixel(renderer, par2.pos.puntos[0], par2.pos.puntos[1], 0, 255, 0, 0);
		
		//cubo.QRotate(rot);
		cubo.Draw(time);
		//cube.Draw(time*5);
		/*SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);*/
		//SDL_RenderDrawPoint(renderer, position, 50 - 20 * sin(position));
	
		SDL_RenderPresent(renderer);
		
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return EXIT_SUCCESS;
}