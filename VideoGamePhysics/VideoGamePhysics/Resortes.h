#pragma once
#include "Vector3.h"
#include "Particula.h"


class ResorteA
{
public:
	float k;
	float b;
	CVector3 puntoF;
	CVector3 puntoE;
	/*CParticula afecta;*/
	CParticula *afecta;
	void fuerzaRe();

	/*ResorteA(float _k, float _b, CVector3 p, CParticula *a);*/
	ResorteA(float _k, float _b, CVector3 p, CParticula &a);
	~ResorteA();
};

class ResorteB
{
public:
	float k;
	float b;
	CVector3 puntoE;
	//CParticula afecta2;
	//CParticula ancla;

	CParticula *afecta2;
	CParticula *ancla;
	void fuerzaRe2();

	ResorteB(float _k, float _b, CParticula &_ancla, CParticula &a);
	/*ResorteB(float _k, float _b, CParticula *_ancla, CParticula *a);*/
	~ResorteB();
};