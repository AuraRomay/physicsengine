#include "Matrix3.h"



Matrix3::Matrix3()
{
}


Matrix3::~Matrix3()
{
}

Matrix3::Matrix3(CVector3 v1, CVector3 v2, CVector3 v3)
{
	vec[0] = v1;
	vec[1] = v2;
	vec[2] = v3;

}

void Matrix3::mTranspose()
{
	CVector3 tempV1 = CVector3();
	CVector3 tempV2 = CVector3();
	CVector3 tempV3 = CVector3();
	/*tempV.puntos[0] = this->v1.puntos[0];
	tempV.puntos[1] = this->v2.puntos[0];
	tempV.puntos[2] = this->v3.puntos[0];*/

	tempV1.setPunto(this->v1().puntos[0], this->v2().puntos[0], this->v3().puntos[0]);
	tempV2.setPunto(this->v1().puntos[1], this->v2().puntos[1], this->v3().puntos[1]);
	tempV3.setPunto(this->v1().puntos[2], this->v2().puntos[2], this->v3().puntos[2]);

	this->vec[0] = tempV1;
	this->vec[1] = tempV2;
	this->vec[2] = tempV3;
}

CVector3 Matrix3::operator*(const CVector3 & vectorMul)
{
	CVector3 resultado = CVector3();
	resultado.puntos[0] = 
		this->v1().puntos[0] * vectorMul.puntos[0] +
		this->v2().puntos[0] * vectorMul.puntos[1] +
		this->v3().puntos[0] * vectorMul.puntos[2];
	resultado.puntos[1] =
		this->v1().puntos[1] * vectorMul.puntos[0] +
		this->v2().puntos[1] * vectorMul.puntos[1] +
		this->v3().puntos[1] * vectorMul.puntos[2];
	resultado.puntos[2] = 
		this->v1().puntos[2] * vectorMul.puntos[0] +
		this->v2().puntos[2] * vectorMul.puntos[1] +
		this->v3().puntos[2] * vectorMul.puntos[2];
	
	return resultado;
}

Matrix3 Matrix3::operator*(const Matrix3 & matrizMul)
{
	Matrix3 resultado = Matrix3();
	Matrix3 tempM = Matrix3();

	tempM.vec[0] = this->vec[0];
	tempM.vec[1] = this->vec[1];
	tempM.vec[2] = this->vec[2];

	tempM.mTranspose();
	resultado.vec[0].puntos[0] = tempM.vec[0].pPunto(matrizMul.vec[0]);
	resultado.vec[0].puntos[1] = tempM.vec[1].pPunto(matrizMul.vec[0]);
	resultado.vec[0].puntos[2] = tempM.vec[2].pPunto(matrizMul.vec[0]);
	resultado.vec[1].puntos[0] = tempM.vec[0].pPunto(matrizMul.vec[1]);
	resultado.vec[1].puntos[1] = tempM.vec[1].pPunto(matrizMul.vec[1]);
	resultado.vec[1].puntos[2] = tempM.vec[2].pPunto(matrizMul.vec[1]);
	resultado.vec[2].puntos[0] = tempM.vec[0].pPunto(matrizMul.vec[2]);
	resultado.vec[2].puntos[1] = tempM.vec[1].pPunto(matrizMul.vec[2]);
	resultado.vec[2].puntos[2] = tempM.vec[2].pPunto(matrizMul.vec[2]);

	return resultado;
}
