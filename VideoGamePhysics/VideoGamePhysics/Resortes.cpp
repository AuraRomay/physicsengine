#include "Resortes.h"


//ResorteA::ResorteA(float _k, float _b, CVector3 p, CParticula *a)
//{
//	k = _k;
//	b = _b;
//	puntoF = p;
//	afecta.pos = a.pos;
//}

ResorteA::ResorteA(float _k, float _b, CVector3 p, CParticula &a)
{
	k = _k;
	b = _b;
	puntoF = p;
	afecta = &a;
}

ResorteA::~ResorteA()
{
}

void ResorteA::fuerzaRe()
{
	CVector3 p = afecta->pos;
	CVector3 p0 = puntoF;

	CVector3 A = p - p0;

	CVector3 fuerza = (A * -k) - (afecta->vel * b);
	afecta->addForce(fuerza);
}


//ResorteB::ResorteB(float _k, float _b, CParticula *_ancla, CParticula *a)
//{
//	k = _k;
//	b = _b;
//	ancla.pos = _ancla->pos;
//	afecta2 = a;
//}

ResorteB::ResorteB(float _k, float _b, CParticula &_ancla, CParticula &a)
{
	k = _k;
	b = _b;
	ancla = &_ancla;
	afecta2 = &a;
}

ResorteB::~ResorteB()
{
}

void ResorteB::fuerzaRe2()
{
	CVector3 p = afecta2->pos;
	/*CParticula p0 = ancla;*/

	CVector3 A = p - ancla->pos;

	CVector3 fuerza = ((A)* -k) - (afecta2->vel * b);
	afecta2->addForce(fuerza);
	//std::cout << fuerza.puntos[1] << std::endl;
}
