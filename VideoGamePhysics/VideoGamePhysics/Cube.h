#pragma once
#include "Vector3.h"
#include "Primitive.h"

class Cube:public Primitive
{
public:

	Cube(SDL_Renderer *rend, CVector3 centro, int size);
	void getVertices();
	~Cube();
};

