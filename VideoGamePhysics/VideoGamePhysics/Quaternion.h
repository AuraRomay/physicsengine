#pragma once
#include "Vector3.h"

class Quaternion
{
public:
	Quaternion();
	Quaternion(CVector3 v, float e);
	~Quaternion();

	static Quaternion identity;

	Quaternion & operator +(Quaternion rh);
	Quaternion & operator -(Quaternion rh);
	Quaternion & operator *(float rh);
	Quaternion & operator *(Quaternion rh);
	void Normalize();
	float Magnitud();
	void Conjugate();

	float real;
	CVector3 ijk;
};

