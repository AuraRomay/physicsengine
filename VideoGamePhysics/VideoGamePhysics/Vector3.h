#pragma once
#include <vector>
#include <string.h>
#include <iostream>
#include <math.h>

class CVector3
{
public:
	CVector3();
	~CVector3();
	CVector3(float, float, float);

	float puntos[3];
	float x() { return puntos[0]; }
	float y() { return puntos[1]; }
	float z() { return puntos[2]; }

	void setPunto(float x, float y, float z);

	CVector3 operator+(const CVector3 &vec);
	CVector3 operator-(const CVector3 &vec);
	CVector3 operator*(const CVector3 &vec);
	CVector3 operator*(const float &num);
	CVector3 proyVectorial(CVector3 vec); //this->vector se proyecta en parametro

	void operator+=(const CVector3 &vec);
	void operator-=(const CVector3 &vec);
	void operator*=(const CVector3 &vec);
	void operator*=(const float &num);

	float proyEscalar(CVector3 vec);	//this->vector se proyecta en parametro
	float pPunto(const CVector3 &vec);
	float magnitud();
	CVector3 normalizar();

	void imprimir();
};

