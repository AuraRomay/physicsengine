#pragma once
#include "Vector3.h"
#include "Primitive.h"

class Triangulo: public Primitive
{
public:
	Triangulo(SDL_Renderer *rend, CVector3 centro, int size);
	void getVertices();
	~Triangulo();
};

