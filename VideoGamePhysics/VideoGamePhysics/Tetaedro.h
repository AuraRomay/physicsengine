#pragma once
#include "Vector3.h"
#include "Primitive.h"

class Tetaedro: public Primitive
{
public:
	Tetaedro(SDL_Renderer *rend, CVector3 centro, int size);
	void getVertices();
	~Tetaedro();
};

