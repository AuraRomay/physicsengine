#include "Particula.h"
#include <assert.h>

CParticula::CParticula()
{
	if(masa > 0)
		masaInversa = 1 / masa;
}

CParticula::CParticula(float _masa, CVector3 _pos, CVector3 _vel)
{
	masa = _masa;
	pos = _pos;
	vel = _vel;

	if (masa > 0)
		masaInversa = 1 / masa;
}


CParticula::~CParticula()
{
}

void CParticula::set_pixel(SDL_Renderer * rend, int x, int y, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	SDL_SetRenderDrawColor(rend, r, g, b, a);
	SDL_RenderDrawPoint(rend, x, y);
}

void CParticula::Integrate(float deltaTime)
{
	pos = pos + vel * deltaTime;
	acelera = forceAcum * masaInversa;
	vel = vel + acelera * deltaTime;
	//vel = vel * Reductor;
	//std::cout << vel.puntos[1] <<std::endl;
	forceAcum.setPunto(0,0,0);
}

void CParticula::addForce(CVector3 fuerza)
{
	forceAcum = forceAcum + fuerza;
}//Hola Aura!
