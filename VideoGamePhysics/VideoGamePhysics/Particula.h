#pragma once
#include "Vector3.h"
#include "SDL.h"

class CParticula
{
public:
	CParticula();
	CParticula(float _masa, CVector3 _pos, CVector3 _vel);
	~CParticula();
	void set_pixel(SDL_Renderer *rend, int x, int y, Uint8 r, Uint8 g, Uint8 b, Uint8 a);
	void Integrate(float deltaTime);
	void addForce(CVector3 fuerza);

	float masa;
	float masaInversa; 
	CVector3 pos;
	CVector3 vel;
	CVector3 acelera;
	CVector3 forceAcum;

	float Reductor = 0.9999f;
};

