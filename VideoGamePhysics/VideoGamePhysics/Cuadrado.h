#pragma once
#include "Vector3.h"
#include "Primitive.h"

class Cuadrado :public Primitive
{
public:

	Cuadrado(SDL_Renderer *rend, CVector3 centro, int size);
	void getVertices();
	~Cuadrado();
};
