#include "Quaternion.h"
#include <math.h>


Quaternion::Quaternion()
{
	ijk = CVector3(0, 0, 0);
	real = 1;
}

Quaternion::Quaternion(CVector3 v, float e)
{
	ijk = v;
	real = e;
}


Quaternion::~Quaternion()
{
}

Quaternion & Quaternion::operator+(Quaternion rh)
{
	Quaternion res;
	res.real = real + rh.real;

	res.ijk = ijk + rh.ijk;

	return res;
}

Quaternion & Quaternion::operator-(Quaternion rh)
{
	Quaternion res;
	res.real = real - rh.real;

	res.ijk = ijk - rh.ijk;

	return res;
}

Quaternion & Quaternion::operator*(float rh)
{
	Quaternion res;
	res.ijk = ijk * rh;
	res.real = real * rh;

	return res;
}

Quaternion & Quaternion::operator*(Quaternion rh)
{
	Quaternion res;
	res.real = real * rh.real - ijk.pPunto(rh.ijk);
	res.ijk = rh.ijk*real + ijk * rh.real + ijk * rh.ijk;

	return res;
}

void Quaternion::Normalize()
{
	float r;
	r = 1 / (sqrt(pow(ijk.puntos[0], 2) + pow(ijk.puntos[1], 2) + pow(ijk.puntos[2], 2) + pow(real, 2)));
	real = real * r;
	ijk = ijk * r;
}

float Quaternion::Magnitud()
{
	float m;
	m = sqrt(pow(ijk.puntos[0], 2) + pow(ijk.puntos[1], 2) + pow(ijk.puntos[2], 2) + pow(real, 2));
	return m;
}

void Quaternion::Conjugate()
{
	ijk = ijk * -1;
}
