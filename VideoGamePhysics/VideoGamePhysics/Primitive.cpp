#include "Primitive.h"
#include <math.h>



void Primitive::QRotate(Quaternion q)
{
	float angle = (q.real * 3.1416) / 180;

	q.Normalize();
	CVector3 q1v = q.ijk * sin(angle);
	Quaternion q1 (q1v, cos(angle));
	Quaternion qc = q1;

	qc.Conjugate();
	
	/*Quaternion conjugado = q;
	conjugado.Conjugate();*/

	for (int i = 0; i < vertices.size(); i++) {
		vertices[i] = vertices[i] - position;
		Quaternion trans;
		trans.real = 0;
		trans.ijk = vertices[i];

		Quaternion temp;
	
		temp = trans * qc;
		temp = q1 * temp;

		vertices[i] = temp.ijk;
		vertices[i] = vertices[i] + position;
	}

}

Primitive::~Primitive()
{
}
