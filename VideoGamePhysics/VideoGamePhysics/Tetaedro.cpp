#include "Tetaedro.h"
#include <iostream>



Tetaedro::Tetaedro(SDL_Renderer *rend, CVector3 centro, int size) :Primitive(rend, centro, size)
{
}


void Tetaedro::getVertices()
{
	float altura = std::sin(60 * 3.14159265358979323846 / 180) * size;
	float radio = (2 * altura) / 3;
	float ap = altura - radio;
	float newSi = size;

	float cambioY = (size*std::sqrtf(6)) / 3;
	float apY = (size*std::sqrtf(6)) / 12;

	CVector3 punto1(newSi * 0.5764 , newSi * 0, newSi * -0.2041);
	punto1 = punto1 + position;
	CVector3 punto2(newSi * -0.2887, newSi * 0.5, newSi * -0.2041);
	punto2 = punto2 + position;
	CVector3 punto3(newSi * -0.2887, newSi * -0.5, newSi * -0.2041);
	punto3 = punto3 + position;
	CVector3 punto4(newSi * 0, newSi * 0, newSi * 0.6124);
	punto4 = punto4 + position;

	vertices.push_back(punto1);
	vertices.push_back(punto2);
	vertices.push_back(punto3);
	vertices.push_back(punto4);

	CVector3 temp1 = punto1 - punto2;
	CVector3 temp2 = punto1 - punto3;
	CVector3 temp3 = punto1 - punto4;
	CVector3 temp4 = punto2 - punto3;
	CVector3 temp5 = punto2 - punto4;
	CVector3 temp6 = punto3 - punto4;
	std::cout << "MAG" << temp1.magnitud()<<std::endl;
	std::cout << "MAG" << temp2.magnitud() << std::endl;
	std::cout << "MAG" << temp3.magnitud() << std::endl;
	std::cout << "MAG" << temp4.magnitud() << std::endl;
	std::cout << "MAG" << temp5.magnitud() << std::endl;
	std::cout << "MAG" << temp6.magnitud() << std::endl;
}

Tetaedro::~Tetaedro()
{
}
