#include "Cuadrado.h"

Cuadrado::Cuadrado(SDL_Renderer * rend, CVector3 centro, int size) :Primitive(rend, centro, size)
{
}

void Cuadrado::getVertices()
{
	float newSi = size / 2;
	CVector3 punto1(position.x() - newSi, position.y() - newSi, 0);
	CVector3 punto2(punto1.x() - size, punto1.y(), punto1.z());
	CVector3 punto3(punto2.x(), punto2.y() + size, punto2.z());
	CVector3 punto4(punto3.x() + size, punto3.y(), punto3.z());

	vertices.push_back(punto1);
	vertices.push_back(punto2);
	vertices.push_back(punto3);
	vertices.push_back(punto4);
}

Cuadrado::~Cuadrado()
{
}
