#pragma once
#include "Vector3.h"
#include "Quaternion.h"
#include <vector>
#include "SDL.h"

class Primitive
{
public:

	SDL_Renderer *renderer;
	CVector3 position;
	std::vector<CVector3> vertices;
	int size;

	Primitive(SDL_Renderer *rend, CVector3 _position, int _size)
	{
		renderer = rend;
		position = _position;
		size = _size;
	}

	void Draw(float time) {
		float porcentaje = (104 * size) / 100;
		int rojo;
		int azul;
		int verde;

		

		for (int i = 0; i < vertices.size(); i++) {
			for (int j = i + 1; j < vertices.size(); j++) {
				CVector3 temp = vertices[i] - vertices[j];
				if (temp.magnitud() <= porcentaje) {
					SDL_SetRenderDrawColor(renderer, 120 - 120 * sin(time), 120 - 120 * sin(time * 2), 120 - 120 * sin(time / 2), 255);
					SDL_RenderDrawLine(renderer, vertices[i].x(), vertices[i].y(), vertices[j].x(), vertices[j].y());
				}
			}
		}


		
	}

	void DrawC() {

	}

	void QRotate(Quaternion q);

	/*void DrawCuad()
	{
		SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
		int sizefactor = size / 2;

		SDL_RenderDrawLine(renderer, position.x() - sizefactor, position.y() - sizefactor, position.x() + sizefactor, position.y() - sizefactor);
		SDL_RenderDrawLine(renderer, position.x() + sizefactor, position.y() - sizefactor, position.x() + sizefactor, position.y() + sizefactor);
		SDL_RenderDrawLine(renderer, position.x() + sizefactor, position.y() + sizefactor, position.x() - sizefactor, position.y() + sizefactor);
		SDL_RenderDrawLine(renderer, position.x() - sizefactor, position.y() + sizefactor, position.x() - sizefactor, position.y() - sizefactor);
	}*/

	~Primitive();
};

